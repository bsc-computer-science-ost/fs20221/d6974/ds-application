const express = require("express");
import * as http from 'http';
import * as WebSocket from "ws";
import { board } from './src/classes';
import {GetBoard, updateBoard} from "./controller/dbController";

const PORT = 8080;
const app = express();

app.use(express.static('public'));

const server = http.createServer(app);
const wss = new WebSocket.Server({server});

class updateMessage {
    constructor (
    public type: string,
    public boardId: string,
    public boardData: board){}
}

// Toggle logs
const logs = true;

wss.on('connection',ws => {
    logs && console.log("connection open");
    ws.on('message', data => {
        try {
            let request = JSON.parse(data.toString());
            logs && console.log("\n[Request] type: "+ request.type + ", Board: " + request.boardId);

            if (request.type == 'get') {
                logs && console.log("    [Get-Request Board-ID:] " + request.boardId);
                // Get Data from db
                GetBoard(request.boardId).then((board)=>{
                    // Generate Answer
                    let answer = new updateMessage("update", request.boardId, board);

                    logs && console.log("    [Get-Answer type & Board-ID:] type: "+ answer.type + ", Board: " + answer.boardId);

                    ws.send(JSON.stringify(answer))
                });
            }
            if (request.type == "update") {
                logs && console.log("    [Update-Request Board-ID:] " + request.boardId);
                // Update Data to db
                updateBoard(request.boardData).then(()=>{
                    // Generate Message
                    let answer = new updateMessage("update", request.boardId, request.boardData);

                    logs && console.log("    [Update-To-All type & Board-ID:] type: "+ answer.type + ", Board: " + answer.boardId);

                    wss.clients.forEach(function each(client) {
                        if (client !== ws && client.readyState === WebSocket.OPEN) {
                            client.send(JSON.stringify(answer));
                        }
                    })
                });
            }
        } catch (err) {
            console.error(err)
        }
    })
})

server.listen(PORT, () => console.log(`Server listening on port: ${PORT}`));
