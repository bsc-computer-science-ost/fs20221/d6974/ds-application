# TFOS - Time for other stuff

The application consists of a database two application server and a load balancer. The load balancer is configured to server in hot standby mode.

## Start Application
In order to start the application the load balancer and the backend application image has to be build.

```
cd ds-application/
docker build -t nginxapp deployment/loadbalancer/
docker build -t ds/app:v1 .
```

Since the fronted react application gets delivered with a nginx web server we need to open the frontend repository and also build the frontend container.
```
cd ds-frontend/
docker build -t ds-webapp:v1 .
```

After the build has finished sucessfully, the docker compose solution can be started. The solution is located in the ds-application repostory.
```
cd ds-application/
docker-compose -f deployment/docker-compose.yaml up
```
