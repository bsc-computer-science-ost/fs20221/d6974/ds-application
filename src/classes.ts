export interface board {
    _id : string,
    title : string,
    buckets?: bucket[]
}

export interface bucket {
    title : string,
    items? : item[]
}

export interface item {
    title : string,
    description?: string,
    color?: string,
    tags?: tag[]
}

export interface tag {
    title : string
}