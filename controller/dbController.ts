import {Schema, model, connect} from 'mongoose';
import {board, bucket, item, tag} from '../src/classes';
const url = 'mongodb://ds-database:27017/TFOSdb';

const tagSchema = new Schema<tag>({
    title: String
});

const itemSchema = new Schema<item>({
    title : String,
    description: String,
    color: String,
    tags: [tagSchema]
});

const bucketSchema = new Schema<bucket>({
    title: String,
    items: [itemSchema]
});

const boardSchema = new Schema<board>({
    _id: {type: String, required: true},
    title: {type: String, required: true},
    buckets: [bucketSchema]
});

const Board = model<board>('Board', boardSchema);

async function createBoard(boardId : string){
    await connect(url);
    let board = new Board();
    board._id = boardId;
    board.title = "newBoard";
    await board.save();
    return board;
}

export async function updateBoard(boardobj:board){
    await connect(url);
    await Board.replaceOne({_id: boardobj._id}, {buckets: boardobj.buckets, title: boardobj.title});
    console.log("    [DB-Controller:] Updated Board "+ boardobj._id)
}

export async function GetBoard(boardId : string) {
    await connect(url);
    let result = await Board.findById(boardId).exec();
    if(result == null){
        result = await createBoard(boardId);
        console.log("    [DB-Controller:] New Board " + result._id + " created.");
    }
    return result as board;
}
